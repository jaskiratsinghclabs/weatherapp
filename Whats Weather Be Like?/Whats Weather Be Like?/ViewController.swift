//
//  ViewController.swift
//  Whats Weather Be Like?
//
//  Created by Click Labs 65 on 1/21/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var imageField: UIImageView!
    
    @IBAction func findButton(sender: AnyObject) {
        self.view.endEditing(true)
        
        var tempUrl = "http://www.weather-forecast.com/locations/" + cityField.text.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest"  //removing spaces to further specify into url
        var url = NSURL(string: tempUrl)  //making it as NSURL
        var task = NSURLSession.sharedSession().dataTaskWithURL(url) {
            (data , response , error) in
            var downloadedContent = (NSString(data: data , encoding: NSUTF8StringEncoding)) //website content into variable
            
            //below condn checks whether data is present or not in order to check if city exists or not
            
            if downloadedContent.containsString("<span class=\"phrase\">") {
                
                var extractContent = downloadedContent.componentsSeparatedByString("<span class=\"phrase\">") // separates by string specified in order to get required data
                var newExtractContent = extractContent[1].componentsSeparatedByString("</span>") //cleaning of data
                
                var image : UIImage = UIImage(named: "background")
                if newExtractContent[0].containsString("sunny"){
                     image = UIImage(named: "sunny")
                }else if newExtractContent[0].containsString("snow"){
                     image = UIImage(named: "snow")
                }else if newExtractContent[0].containsString("rain"){
                     image = UIImage(named: "rainy")
                }else if newExtractContent[0].containsString("cloudy"){
                    image = UIImage(named: "cloudy")
                }else{
                    image = UIImage(named: "background")
                }
                
                
                dispatch_async(dispatch_get_main_queue()){      //to update label as fast as possible
                    self.message.text = newExtractContent[0].stringByReplacingOccurrencesOfString("&deg;", withString: "°") as String           //replaces degree fileds with sign
                    self.detailsButton.hidden = false
                    self.imageField.image = image
                }
            }else{
                //if city is not found
                dispatch_async(dispatch_get_main_queue()){
                    self.message.text = "Not A Valid City"
                }
            }
        }
        task.resume()       //task is started
    }
    
    func shouldAutoRotate() -> Bool { //stoping auto rotate
        return false;
    }
    
    override func supportedInterfaceOrientations() -> Int {  //setting to portrait
        return UIInterfaceOrientationMask.Portrait.toRaw().hashValue
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) { //to show details on next window
        var destViewController : SecondViewController = segue.destinationViewController as SecondViewController
        var newUrl = "http://www.weather-forecast.com/locations/" + cityField.text.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest/threedayfree"
        destViewController.urlPath = newUrl
    }
    
}


