//
//  SecondViewController.swift
//  Whats Weather Be Like?
//
//  Created by Click Labs 65 on 1/21/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import Foundation
import UIKit

class SecondViewController : UIViewController {
    @IBOutlet weak var webView: UIWebView!
    var urlPath = String()  //updated from first view control
    
    func shouldAutoRotate() -> Bool { // switching off auto rotate
        return false;
    }
    
    override func supportedInterfaceOrientations() -> Int { // setting orientation to right
        return UIInterfaceOrientationMask.LandscapeRight.toRaw().hashValue
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var url  = NSURL(string: urlPath)
        let request = NSURLRequest(URL: url)
        webView.loadRequest(request) // fills the web view
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}